(use-modules (guix packages))
(use-modules (guix gexp))
(use-modules (gnu packages))
(use-modules (guix build-system trivial))
(use-modules (guix build-system asdf))
(use-modules (guix profiles))
(use-modules (guix git-download))
(use-modules (gnu packages lisp))
(use-modules (gnu packages lisp-check))
(use-modules (gnu packages lisp-xyz))
(use-modules (gnu packages readline))
(use-modules (ice-9 popen))
(use-modules (ice-9 textual-ports))
(use-modules (ice-9 rdelim))
(use-modules ((guix licenses) #:prefix license:))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define (skip-git-and-build-directory file stat)
  "Skip the `.git` and `build` and `guix_profile` directory when collecting the sources."
  (let ((name (basename file)))
    (not (or (string=? name ".git")
             (string=? name "build")
             (string-prefix? "guix_profile" name)))))

(define *version* (call-with-input-file "VERSION"
                    get-string-all))

(define-public sbcl-granivore
  (package
   (name "sbcl-granivore")
   (version (git-version *version* "HEAD" %git-commit))
   (source (local-file %source-dir
                       #:recursive? #t
                       #:select? skip-git-and-build-directory))
    (inputs
     (list sbcl rlwrap cl-asdf
           sbcl-slime-swank))
    (build-system asdf-build-system/sbcl)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((asdf (assoc-ref inputs "cl-asdf"))
                   (out (assoc-ref outputs "out")))
               (setenv "HOME" (getcwd))
               (setenv "ASDF_DIR" asdf)
               (invoke "./build.sh")
               (mkdir-p (string-append out "/bin"))
               (copy-file "granivore" (string-append out "/bin/granivore"))))))))
    (synopsis "Find cryptographic seeds. ")
    (description
     "Granivore detects BIP-39 words on its standard input, and outputs the byte number at which a word was found, alongside with the word.

It can be used during the forensics analysis of a device, in order to detect a cryptocurrency seed.

It is named after after the adjective that applies to animals that eat seeds.")
    (home-page "https://gitlab.com/edouardklein/granivore")
    (license license:agpl3+)))

sbcl-granivore

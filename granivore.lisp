(defpackage :granivore
  (:use :cl)
  (:export :matching-automaton :to-dot :build-for-dict))
(in-package :granivore)

(defun sym->str (sym)
  "Return the string representation of a : symbol, e.g. :toto -> \"toto\""
  (string-downcase sym))

(defun str->sym (s)
  "Return the keyword symbol based on s, e.g. \"toto\" -> :toto.
If s is already a symbol, return it. Also handles characters"
  (cond
    ((typep s 'keyword) s)
    ((typep s 'string) (read-from-string (concatenate 'string ":" s)))
    ((typep s 'standard-char) (str->sym (string s)))
    (t (error "What is the symbol for ~a, of type ~a ?~%" s (type-of s)))))

(defun matching-automaton (patterns)
  "Return the automaton that will match the given patterns.

The automaton is built with three passes.

The first pass defines all the states the automaton will have.

These states are all the possible prefixes of the given patterns, from the empty string up to the complete patterns themselves.

Then the second pass defines the transitions between states where each letter let us go from one existing state to another.

Finally, the last pass lets the automaton fallback on a partial match when
- a word is accepted (a pattern has been found) (:accept)
- the letter it just got breaks all the words it could build (:reject)

e.g. with patterns banana and nani, we should go banan-i->nani, and not banan-i->:start-state."
  (let ((answer '()))  ;; The automaton is a plist where keys are states
    ;; and the values are the transitions
    ;; First pass
    (dolist (pattern patterns)
      ;;(format t "Adding pattern ~a to the automaton~%" pattern)
      (loop
        for i from 1 to (length pattern)
        do (let ((state (str->sym (subseq pattern 0 i))))
             ;;(format t "~tAdding state ~a to the automaton~%" state)
             (setf (getf answer state) '()))))
    ;; Second pass
    (dolist (pattern patterns)
      ;;(format t "Adding the state transitions for pattern ~a~%" pattern)
      (loop
        for i from 0 to (1- (length pattern))
        do (let ((start-state (if (= 0 i)
                                  :start-state
                                  (str->sym (subseq pattern 0 i))))
                 (event (str->sym (subseq pattern i (1+ i))))
                 (end-state (str->sym (subseq pattern 0 (1+ i)))))
             ;;(format t "~tAdding transition from ~a to ~a on event ~a~%" start-state end-state event)
             (setf (getf (getf answer start-state) event) end-state))))
    ;; Third pass
    ;;(format t "Adding fallback transitions for all states~%")
    ;; :initial-next-char is the special tag before :start-state where next-char
    ;; is called for the first time
    (setf (getf (getf answer :start-state) :reject) :initial-next-char)
    (let ((states (loop for state in answer by 'cddr
                        unless (eql state :start-state)
                          collect (sym->str state))))
      (dolist (state states)
        ;;(format t "~tFor state ~a, patterns ~a~%" state patterns)
        (let ((key (if (find state patterns :test #'equal)
                       ;; If the current state is a complete pattern,
                       ;; add an :accept property (it should be the only property
                       ;; in that state's transition list)
                       ;; otherwise, add a catch-all :reject property
                       :accept
                       :reject))
              (value
                ;; Loop through all the suffixes of state, from longest to shortest
                ;; Return the first (and therefore longest) one that is also a state
                ;; This state will be the destination state when either accept a word
                ;; or get a new character that is incompatible with the patterns
                (loop for i from 1 to (1- (length state))
                      if (some (lambda (x) (string= x (subseq state i))) states)
                        return (str->sym (subseq state i))
                      finally (return :start-state))))
          ;;(format t "For state ~a, key ~a -> ~a~%" state key value)
          (setf (getf (getf answer (str->sym state)) key) value))))
    answer))

(defparameter *automaton* (matching-automaton '("able" "blast")))

(defun to-dot (automaton)
  "Prints a DOT-language representation of the given automaton"
  (format t "digraph G {~%")
  (loop for start-state in automaton by 'cddr  ;; plist's keys
        do (let ((outgoing (getf automaton start-state)))
             (format t "\"~a\";~%" start-state)
             (loop for event in outgoing by 'cddr
                   do (let ((destination (getf outgoing event)))
                        (cond
                          ((string= event :reject) (format t "\"~a\" -> \"~a\" [color=\"red\"];~%" start-state destination))
                          ((string= event :accept) (format t "\"~a\" -> \"~a\" [color=\"green\"];~%" start-state destination))
                          (t (format t "\"~a\" -> \"~a\" [label=\"~a\"];~%" start-state destination event)))))))
  (format t "}~%"))

(defmacro state-body (name transitions)
  "The tagged instructions that implement one state of the finite state automata"
  `(,(str->sym name)
    ;;(format t "~a ~a~%" (quote ,name) *x*)
    ,@(if (getf transitions :accept)
         ;; If we have an accept in transitions, then it is assumed it is the only
         ;; transition in there, so we just output the match and switch the the fallback state
         `((got-a-match (string (quote ,name))) (go ,(str->sym (getf transitions :accept))))
         ;; Otherwise, we assume we have in :reject in transitions
         `((cond
             ;; Check for end of input
             ((null *x*) (return :EOI))
             ;; Collect all the transitions to another state
             ,@(loop for event in transitions by 'cddr
                     unless (string= event :reject)
                       collect (let ((next-state (getf transitions event)))
                                 `((char-equal *x* ,(coerce event 'character)) (next-char) (go ,(str->sym next-state)))))
             ;; Now put the catch-all transition
             (t (go ,(str->sym (getf transitions :reject)))))))))

(defmacro automaton-prog (automaton)
  "The tagbody which groups all the states of the finite state automata"
  `(prog ()
    :initial-next-char
      (next-char)
      ,@(loop for state in automaton by 'cddr
              append (macroexpand `(state-body ,state ,(getf automaton state))))))

;; Reading the stream is done with global variables,
;; *x* being the char at offset *pos*
(defparameter *pos* (- 1))

(defparameter *x* nil)

(defparameter *input-stream* *standard-input*)

(defun next-char ()
  "Read the next character from the input-stream and put it in *x*"
  (incf *pos*)
  (setf *x* (let ((b (read-byte *input-stream* nil)))
              (if b
                  (code-char b)
                  b))))

(defun got-a-match (s)
  (format t "~a ~a~%" *pos* s))

(defmacro make-find-patterns (patterns)
  "Define the functions that will find the patterns"
  `(progn
       (defun find-patterns ()
         "Find the patterns in *input-stream*"
         ,(macroexpand `(automaton-prog ,(matching-automaton patterns))))
      (defun find-patterns-in-string (s)
        "Find the patterns in s"
        (with-input-from-string (*input-stream* s)
          (find-patterns)))))


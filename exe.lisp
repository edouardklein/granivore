(in-package :granivore)

(defun build-for-dict (&optional (dict "english.txt"))
  (eval
   `(make-find-patterns
     ,(with-open-file (in dict)
        (loop for line = (read-line in nil)
              while line
              collect line))))
  (sb-ext:save-lisp-and-die #p"granivore" :toplevel #'granivore::find-patterns
                                          :executable t))

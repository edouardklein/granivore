#!/usr/bin/env bash
set -euxo pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/"
if [ -z ${ASDF_DIR+x} ]
then
    ASDF_DIR="$GUIX_ENVIRONMENT"
fi
ASDF_OUTPUT_TRANSLATIONS="/:$HOME/.cache/common-lisp/my-dir/" sbcl \
    --dynamic-space-size 1048576 \
    --non-interactive \
    --load "$ASDF_DIR"/share/common-lisp/source/asdf/asdf.lisp \
    --eval  '(setf asdf:*central-registry* '"'"'(#P"'$SCRIPT_DIR'"))' \
    --eval '(asdf:operate '"'"'asdf:load-op :granivore)' \
    --eval '(granivore:build-for-dict)'
